package com.example.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Category implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idCategory;
	private String code;
	private String Designation;
	@OneToMany(mappedBy = "category")
	private List<Article>articles;
	
	public Category() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Category(String code, String designation, List<Article> articles) {
		super();
		this.code = code;
		Designation = designation;
		this.articles = articles;
	}

	public Long getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(Long idCategory) {
		this.idCategory = idCategory;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesignation() {
		return Designation;
	}

	public void setDesignation(String designation) {
		Designation = designation;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
	
	
	
	

}
