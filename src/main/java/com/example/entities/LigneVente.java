package com.example.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class LigneVente implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idLigneVente;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idArticle")
	private Article article;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idVente")
	private Vente vente;

}