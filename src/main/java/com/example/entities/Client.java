package com.example.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
@Entity
public class Client implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idClient;
	private String nomClient;
	private String prenomsClient;
	private String adresseClient;
	@Email
	private String emailClient;
	private String photoClient;
	
	@OneToMany(mappedBy = "client")
	private List<CommandeClient> commandeClients;
	
	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Client(String nomClient, String prenomsClient, String adresseClient, String emailClient,
			String photoClient) {
		super();
		this.nomClient = nomClient;
		this.prenomsClient = prenomsClient;
		this.adresseClient = adresseClient;
		this.emailClient = emailClient;
		this.photoClient = photoClient;
	}

	public Long getIdClient() {
		return idClient;
	}

	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public String getPrenomsClient() {
		return prenomsClient;
	}

	public void setPrenomsClient(String prenomsClient) {
		this.prenomsClient = prenomsClient;
	}

	public String getAdresseClient() {
		return adresseClient;
	}

	public void setAdresseClient(String adresseClient) {
		this.adresseClient = adresseClient;
	}

	public String getEmailClient() {
		return emailClient;
	}

	public void setEmailClient(String emailClient) {
		this.emailClient = emailClient;
	}

	public String getPhotoClient() {
		return photoClient;
	}

	public void setPhotoClient(String photoClient) {
		this.photoClient = photoClient;
	}

	public List<CommandeClient> getCommandeClients() {
		return commandeClients;
	}

	public void setCommandeClients(List<CommandeClient> commandeClients) {
		this.commandeClients = commandeClients;
	}
}
