package com.example.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class LigneCommandeFournisseur implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idLigneCommandeFournisseur;
	
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idcommandeFournisseur")
	private CommandeFournisseur commandeFournisseur;

	public LigneCommandeFournisseur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LigneCommandeFournisseur(Article article, CommandeFournisseur commandeFournisseur) {
		super();
		this.article = article;
		this.commandeFournisseur = commandeFournisseur;
	}

	public Long getIdLigneCommandeFournisseur() {
		return idLigneCommandeFournisseur;
	}

	public void setIdLigneCommandeFournisseur(Long idLigneCommandeFournisseur) {
		this.idLigneCommandeFournisseur = idLigneCommandeFournisseur;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}
}
