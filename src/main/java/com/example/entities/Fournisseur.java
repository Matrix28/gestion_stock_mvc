package com.example.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
@Entity
public class Fournisseur implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idFournisseur;
	private String nomFournisseur;
	private String prenomsFournisseur;
	private String adresseFournisseur;
	@Email
	private String emailFournisseur;
	private String photoFournisseur;
	
	@OneToMany(mappedBy = "fournisseur")
	private List<CommandeFournisseur> commandeFournisseurs;
	
	public Fournisseur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Fournisseur(String nomFournisseur, String prenomsFournisseur, String adresseFournisseur,
			String emailFournisseur, String photoFournisseur) {
		super();
		this.nomFournisseur = nomFournisseur;
		this.prenomsFournisseur = prenomsFournisseur;
		this.adresseFournisseur = adresseFournisseur;
		this.emailFournisseur = emailFournisseur;
		this.photoFournisseur = photoFournisseur;
	}

	public Long getIdFournisseur() {
		return idFournisseur;
	}

	public void setIdFournisseur(Long idFournisseur) {
		this.idFournisseur = idFournisseur;
	}

	public String getNomFournisseur() {
		return nomFournisseur;
	}

	public void setNomFournisseur(String nomFournisseur) {
		this.nomFournisseur = nomFournisseur;
	}

	public String getPrenomsFournisseur() {
		return prenomsFournisseur;
	}

	public void setPrenomsFournisseur(String prenomsFournisseur) {
		this.prenomsFournisseur = prenomsFournisseur;
	}

	public String getAdresseFournisseur() {
		return adresseFournisseur;
	}

	public void setAdresseFournisseur(String adresseFournisseur) {
		this.adresseFournisseur = adresseFournisseur;
	}

	public String getEmailFournisseur() {
		return emailFournisseur;
	}

	public void setEmailFournisseur(String emailFournisseur) {
		this.emailFournisseur = emailFournisseur;
	}

	public String getPhotoFournisseur() {
		return photoFournisseur;
	}

	public void setPhotoFournisseur(String photoFournisseur) {
		this.photoFournisseur = photoFournisseur;
	}
}
