package com.example.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
@Entity
public class Utilisateur implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idUtilisateur;
	private String nomUtilisateur;
	private String prenomsUtilisateur;
	private String motDePasseUtilisateur;
	@Email
	private String emailUtilisateur;
	private String photoUtilisateur;
	
	public Utilisateur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Utilisateur(String nomUtilisateur, String prenomsUtilisateur, String motDePasseUtilisateur,
			String emailUtilisateur, String photoUtilisateur) {
		super();
		this.nomUtilisateur = nomUtilisateur;
		this.prenomsUtilisateur = prenomsUtilisateur;
		this.motDePasseUtilisateur = motDePasseUtilisateur;
		this.emailUtilisateur = emailUtilisateur;
		this.photoUtilisateur = photoUtilisateur;
	}

	public Long getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(Long idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public String getNomUtilisateur() {
		return nomUtilisateur;
	}

	public void setNomUtilisateur(String nomUtilisateur) {
		this.nomUtilisateur = nomUtilisateur;
	}

	public String getPrenomsUtilisateur() {
		return prenomsUtilisateur;
	}

	public void setPrenomsUtilisateur(String prenomsUtilisateur) {
		this.prenomsUtilisateur = prenomsUtilisateur;
	}

	public String getMotDePasseUtilisateur() {
		return motDePasseUtilisateur;
	}

	public void setMotDePasseUtilisateur(String motDePasseUtilisateur) {
		this.motDePasseUtilisateur = motDePasseUtilisateur;
	}

	public String getEmailUtilisateur() {
		return emailUtilisateur;
	}

	public void setEmailUtilisateur(String emailUtilisateur) {
		this.emailUtilisateur = emailUtilisateur;
	}

	public String getPhotoUtilisateur() {
		return photoUtilisateur;
	}

	public void setPhotoUtilisateur(String photoUtilisateur) {
		this.photoUtilisateur = photoUtilisateur;
	}
}