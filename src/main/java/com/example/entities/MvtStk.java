package com.example.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class MvtStk implements Serializable{
	
	public static final int ENTREE = 1;
	
	public static final int SORTIE = 2;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idMvtStock;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMvtStock;
	
	private BigDecimal quantiteMvtStock;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idClient")
	private Article article;

	public MvtStk() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MvtStk(Date dateMvtStock, BigDecimal quantiteMvtStock, Article article) {
		super();
		this.dateMvtStock = dateMvtStock;
		this.quantiteMvtStock = quantiteMvtStock;
		this.article = article;
	}

	public Long getIdMvtStock() {
		return idMvtStock;
	}

	public void setIdMvtStock(Long idMvtStock) {
		this.idMvtStock = idMvtStock;
	}

	public Date getDateMvtStock() {
		return dateMvtStock;
	}

	public void setDateMvtStock(Date dateMvtStock) {
		this.dateMvtStock = dateMvtStock;
	}

	public BigDecimal getQuantiteMvtStock() {
		return quantiteMvtStock;
	}

	public void setQuantiteMvtStock(BigDecimal quantiteMvtStock) {
		this.quantiteMvtStock = quantiteMvtStock;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
}