package com.example.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class CommandeFournisseur implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idCommandeFournisseur;
	
	private String codeCommandeFornisseur;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCommandeFornisseur;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idFournisseur")
	private Fournisseur fournisseur;
	
	@OneToMany(mappedBy = "commandeFournisseur")
	private List<LigneCommandeFournisseur> ligneCommandeFournisseurs;

	public CommandeFournisseur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CommandeFournisseur(String codeCommandeFornisseur, Date dateCommandeFornisseur, Fournisseur fournisseur) {
		super();
		this.codeCommandeFornisseur = codeCommandeFornisseur;
		this.dateCommandeFornisseur = dateCommandeFornisseur;
		this.fournisseur = fournisseur;
	}

	public Long getIdCommandeFournisseur() {
		return idCommandeFournisseur;
	}

	public void setIdCommandeFournisseur(Long idCommandeFournisseur) {
		this.idCommandeFournisseur = idCommandeFournisseur;
	}

	public String getCodeCommandeFornisseur() {
		return codeCommandeFornisseur;
	}

	public void setCodeCommandeFornisseur(String codeCommandeFornisseur) {
		this.codeCommandeFornisseur = codeCommandeFornisseur;
	}

	public Date getDateCommandeFornisseur() {
		return dateCommandeFornisseur;
	}

	public void setDateCommandeFornisseur(Date dateCommandeFornisseur) {
		this.dateCommandeFornisseur = dateCommandeFornisseur;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public List<LigneCommandeFournisseur> getLigneCommandeFournisseurs() {
		return ligneCommandeFournisseurs;
	}

	public void setLigneCommandeFournisseurs(List<LigneCommandeFournisseur> ligneCommandeFournisseurs) {
		this.ligneCommandeFournisseurs = ligneCommandeFournisseurs;
	}
}
